function max(...numbers) {
    let result = -Infinity;
    for (let number of numbers) {
        if (number > result) result = number;
    }
    return result;
}
console.log(max(4, 1, 9, -2)); // → 9

let actions = [ "pizza", "running"];
let wolf = {
    colour:"grey",
    were:"true"
};
let day1 = {
    squirrel: false,
    events: ["work", "touched tree", ...actions],
    ...wolf
};
console.log(day1)

