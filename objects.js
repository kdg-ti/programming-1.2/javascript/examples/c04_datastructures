let day1 = {
    squirrel: false,
    events: ["work", "touched tree", "pizza", "running"]
};
console.log(day1.squirrel); // false
console.log(day1.wolf); // undefined
day1.wolf = true;
console.log(day1.wolf); // true
console.log(Object.keys(day1)); // ['squirrel','events','wolf']
console.log(day1["wolf"]); // true
day1.wolf = {
    colour:"grey",
    were:"true"
};
console.log(day1);
//{
//   squirrel: false,
//   events: [ 'work', 'touched tree', 'pizza', 'running' ],
//   wolf: {
//     colour: 'grey',
//     were: 'true'
//   }
//}

console.log(day1.events.includes("pizza")); // true
for (let event of day1.events){
    console.log(event);
}


